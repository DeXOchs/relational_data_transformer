namespace data_transformations {
    public class RelationToForeignObject : Relation {
        public string[] Values { get; }
        public override bool IsEmptyRelation => Values.Length == 0;
        public RelationToForeignObject(string name, string[] values) : base(name)
        {
            Values = values;
        }
    }
}