namespace data_transformations {
    public class RelationToProperty : Relation 
    {
        public string Value { get; }
        public override bool IsEmptyRelation => Value.Equals(string.Empty);
        public RelationToProperty(string name, string value) : base(name) {
            Value = value;
        }
    }
}