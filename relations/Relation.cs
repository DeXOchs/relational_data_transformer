namespace data_transformations {
    public abstract class Relation {
        public string Name { get; }
        public abstract bool IsEmptyRelation {get;}
        protected Relation(string name) {
            Name = name;
        }
    }
}