namespace data_transformations {
    public class RelationToObject : Relation {
        public Subject Value { get; }
        public override bool IsEmptyRelation => Value.HasInvalidResourceIRI;
        public RelationToObject(string name, Subject value) : base(name) {
            Value = value;
        }
    }
}