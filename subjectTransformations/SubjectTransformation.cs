using System.Collections.Generic;
using System.Linq;

namespace data_transformations {
    public class SubjectTransformation : Transformation {
        private Queue<Mapping> mappings = new Queue<Mapping>();

        public void AddMapping(Mapping mapping) => mappings.Enqueue(mapping);

        public List<Subject> Transform(List<string> headers, List<List<DataElement>> data) {
            int numberOfMappings = mappings.Count;
            while (mappings.Count > 0) {
                System.Console.WriteLine($"Processing Subject Mapping ({mappings.Count} of {numberOfMappings} left)");
                Mapping mapping = mappings.Dequeue();
                data = data.Select(row => mapping.Map(headers, row)).ToList();
                headers = mapping.MapHeaders(headers);
            }
            return data.Select(row => ((RelationToObject)row[row.Count - 1].RelationValue).Value).ToList();
        }
    }
}