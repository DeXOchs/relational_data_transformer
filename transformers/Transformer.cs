using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System;

namespace data_transformations {
    public abstract class Transformer<T> {
        protected List<string> headers = new List<string>();
        protected List<List<DataElement>> data = new List<List<DataElement>>();

        #region transformations
        private Queue<TableTransformation> tableTransformations = new Queue<TableTransformation>();
        private SubjectTransformation subjectTransformation;
        private ExportTransformation<T> exportTransformation;
        protected void AddTableTransformation(TableTransformation transformation)
            => tableTransformations.Enqueue(transformation);
        protected void SetSubjectTransformation(SubjectTransformation transformation)
            => subjectTransformation =  transformation;
        protected void SetExportTransformation(ExportTransformation<T> transformation)
            => exportTransformation = transformation;
        #endregion

        public T Transform() {
            List<List<DataElement>> tableTransformedData = new List<List<DataElement>>(data);
            int numberOfTableTransformations = tableTransformations.Count;
            while (tableTransformations.Count > 0) {
                System.Console.WriteLine($"Table Transformation ({tableTransformations.Count} of {numberOfTableTransformations})");
                tableTransformedData = tableTransformations.Dequeue().Transform(headers, tableTransformedData);
            }
            System.Console.WriteLine("Subject Transformation");
            List<Subject> subjectTransformedData = subjectTransformation.Transform(headers, tableTransformedData);
            System.Console.WriteLine($"Export Transformation");
            return exportTransformation.Transform(subjectTransformedData);
        }

        #region helpers
        protected Func<string, string> BuildIRI = input => {
            if (input.Equals(string.Empty)) return string.Empty;
            else return new Guid(MD5.Create().ComputeHash(Encoding.Default.GetBytes(input))).ToString();
        };
        protected Func<string, string> GetFirstName = input => input.Contains(',') ? input.Split(',')[1].Trim() : String.Empty;
        protected Func<string, string> GetLastName = input => input.Contains(',') ? input.Split(',')[0].Trim() : String.Empty;
        #endregion
    }
}