using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic.FileIO;
using System.Text.RegularExpressions;

namespace data_transformations {
    public class TestTransformer : Transformer<string> {
        public TestTransformer() {
            // mock some data here
            headers = new[] { "Application ID", "Application Name", "Application Description", "Application User", "Department" }.ToList();

            data.Add(new[] { "APP-1", "Appname 1", "Description of APP-1", "Max Mustermann", "Dept1" }.Select(value => new DataElement(value)).ToList());
            data.Add(new[] { "APP-2", "Appname 2", "Description of APP-2", "Eva Mustermann", "Dept1" }.Select(value => new DataElement(value)).ToList());

            
            // set mappings
            AddTableTransformation(new CleanDataTableTransformation());

            SubjectTransformation subjectTransformation = new SubjectTransformation();
            
            // first root
            subjectTransformation.AddMapping(new PropertyMapping("ApplicationID", "Application ID", "test:identifier"));
            subjectTransformation.AddMapping(new PropertyMapping("ApplicationDescription", "Application Description", "test:description"));
            subjectTransformation.AddMapping(new PropertyMapping("ApplicationName", "Application Name", "test:name"));
            subjectTransformation.AddMapping(new ObjectMapping(
                "Application",
                "Application ID",
                value => $"test:{BuildIRI(value)}",
                "test:Application",
                new[] {
                    "ApplicationID",
                    "ApplicationName",
                    "ApplicationDescription",
                }
            ));

            // second root as blank node
            subjectTransformation.AddMapping(new PropertyMapping("DepartmentName", "Department", "test:name"));
            subjectTransformation.AddMapping(new ObjectMapping(
                "Department",
                "Department",
                _ => "_:Dept1",
                "test:Department",
                new[] { "DepartmentName" }
            ));

            // third root with relations to Application and Department
            subjectTransformation.AddMapping(new PropertyMapping("UserID", "Application User", "test:identifier"));
            subjectTransformation.AddMapping(new PropertyMapping("UserName", "Application User", "test:name"));
            subjectTransformation.AddMapping(new ForeignKeyMapping(
                "LinkedApplication",
                "Application ID",
                "test:isLinkedTo",
                value => $"test:{BuildIRI(value)}",
                ';'
            ));
            subjectTransformation.AddMapping(new ForeignKeyMapping(
                "LinkedDepartment",
                "Department",
                "test:memberOf",
                _ => "_:Dept1"
            ));
            subjectTransformation.AddMapping(new ObjectMapping(
                "User",
                "Application User",
                value => $"test:{BuildIRI(value)}",
                "test:Person",
                new[] { "UserID", "UserName", "LinkedApplication", "LinkedDepartment" }
            ));

            // virtual node to connect to all roots via forward edge
            subjectTransformation.AddMapping(new ObjectMapping(
                "VirtualRoot",
                new[] { "Application", "Department", "User" }
            ));


            SetSubjectTransformation(subjectTransformation);
            SetExportTransformation(new RDFExportTransformation());
        }
    }
}