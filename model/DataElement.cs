namespace data_transformations {
    public enum ElementType {
        STRING,
        RELATION
    }

    public class DataElement {
        public ElementType Type { get; }
        public string StringValue { get; }
        public Relation RelationValue { get; }
        
        public DataElement(string asString) {
            this.StringValue = asString;
            this.Type = ElementType.STRING;
        }
        public DataElement(Relation asRelation) {
            this.RelationValue = asRelation;
            this.Type = ElementType.RELATION;
        }
    }
}