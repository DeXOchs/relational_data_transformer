using System.Collections.Generic;

namespace data_transformations {
    public class Subject {
        public string ResourceIRI { get; }
        public string TypeIRI { get; }
        public List<Relation> Relations { get; }
        public bool IsBlankNode => ResourceIRI.StartsWith('_');
        public bool IsVirtualNode => TypeIRI.Equals(string.Empty) && ResourceIRI.Equals(string.Empty);
        public bool HasInvalidResourceIRI => ResourceIRI.EndsWith(':');
        
        public Subject(string resourceIRI, string typeIRI, List<Relation> relations) 
        {
            ResourceIRI = resourceIRI;
            TypeIRI = typeIRI;
            Relations = relations;
        }
    }
}