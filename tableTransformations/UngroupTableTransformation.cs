using System.Collections.Generic;
using System;
using System.Linq;

namespace data_transformations {
    public class UngroupTableTransformation : TableTransformation {
        private readonly Queue<Tuple<string, char>> groupedColumns = new Queue<Tuple<string, char>>();

        public void AddGroupedColumnName(string column, char delimiter) 
            => groupedColumns.Enqueue(new Tuple<string, char>(column, delimiter));

        public override List<List<DataElement>> Transform(List<string> headers, List<List<DataElement>> data) {
            // use with caution: not fully tested!
            Queue<Tuple<string, char>> queue = new Queue<Tuple<string, char>>(groupedColumns);
            List<List<DataElement>> ungroupedData = new List<List<DataElement>>();
            while (queue.Count > 0) {
                Tuple<string, char> groupedColumn = queue.Dequeue();
                int columnIndex = headers.IndexOf(groupedColumn.Item1);

                foreach (List<DataElement> row in data) {
                    string grouping = row[columnIndex].StringValue;
                    string[] groupItems = grouping.Split(groupedColumn.Item2);
                    if (groupItems.Length > 1) {
                        // create new rows for each groupItem
                        foreach (string groupItem in groupItems.Skip(1)) {
                            List<DataElement> rowCopy = new List<DataElement>(row);
                            rowCopy[columnIndex] = new DataElement(groupItem);
                            ungroupedData.Add(rowCopy);
                        }
                        // set first groupItem to this row
                        row[columnIndex] = new DataElement(groupItems[0]);
                    }
                }
            }
            return data;
        }
    }
}