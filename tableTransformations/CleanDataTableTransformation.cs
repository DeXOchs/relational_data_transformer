using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;
using System.Linq;

namespace data_transformations {
    public class CleanDataTableTransformation : TableTransformation {
        private Queue<Func<string, string>> cellMappings = new Queue<Func<string, string>>();
        public void AddCellMapping(Func<string, string> mapping)
            => cellMappings.Enqueue(mapping);

        public CleanDataTableTransformation() {
            AddCellMapping(value => value.Trim());
            AddCellMapping(value => Regex.Replace(value, @"\t|\n|\r", " "));
            AddCellMapping(value => value.Replace('"', '\''));
        }
        
        public override List<List<DataElement>> Transform(List<string> _, List<List<DataElement>> data) {
            int numberOfRows = data.Count;
            return data.Select(row =>
                row.Select(cell => {
                    string value = cell.StringValue;
                    if (value != null) {
                        Queue<Func<string, string>> cellMappings = new Queue<Func<string, string>>(this.cellMappings);
                        while (cellMappings.Count > 0) value = cellMappings.Dequeue()(value);
                        return new DataElement(value);
                    }
                    else {
                        return cell;
                    }
                }).ToList()
            ).ToList();
        }
    }
}