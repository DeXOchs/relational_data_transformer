using System.Collections.Generic;

namespace data_transformations 
{
    public abstract class TableTransformation : Transformation 
    {
        public abstract List<List<DataElement>> Transform(List<string> headers, List<List<DataElement>> data);
    }
}