using System.Collections.Generic;
using System;

namespace data_transformations 
{
    public class ColumnTableTransformation : TableTransformation
    {
        private Queue<Tuple<string, Func<string, string>>> columnMappings = new Queue<Tuple<string, Func<string, string>>>();

        public void AddColumnMapping(string columnName, Func<string, string> mapping)
            => columnMappings.Enqueue(new Tuple<string, Func<string, string>>(columnName, mapping));
        
        public override List<List<DataElement>> Transform(List<string> headers, List<List<DataElement>> data) 
        {
            while (columnMappings.Count > 0) 
            {
                Tuple<string, Func<string, string>> columnMapping = columnMappings.Dequeue();
                int i = headers.IndexOf(columnMapping.Item1);
                data.ForEach(row => row[i] = new DataElement(columnMapping.Item2(row[i].StringValue)));
            }
            return data;
        }
    }
}