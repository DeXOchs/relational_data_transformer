using System.Collections.Generic;
using System;
using System.Linq;

namespace data_transformations 
{
    public class FilterTableTransformation : TableTransformation
    {
        private List<Tuple<bool, string, Predicate<string>>> filters = new List<Tuple<bool, string, Predicate<string>>>();

        public void AddInclusionFilter(string columnName, Predicate<string> filter)
            => filters.Add(new Tuple<bool, string, Predicate<string>>(false, columnName, filter));
        
        public void AddExclusionFilter(string columnName, Predicate<string> filter)
            => filters.Add(new Tuple<bool, string, Predicate<string>>(true, columnName, filter));
        
        public override List<List<DataElement>> Transform(List<string> headers, List<List<DataElement>> data) 
        {
            // filters each row for cell value and corrects boolean result according to filter type
            // (inclusionFilter => XOR with false; exclusionFilter => XOR with true)
            return data.FindAll(row => filters
                .Select(filter => filter.Item1 ^ filter.Item3(row[headers.IndexOf(filter.Item2)].StringValue))
                .All(value => value));
        }
    }
}