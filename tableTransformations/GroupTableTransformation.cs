using System.Collections.Generic;
using System;
using System.Linq;

namespace data_transformations {
    public class GroupTableTransformation : TableTransformation {
        private readonly Queue<Tuple<string, char, string[]>> ungroupedColumns = new Queue<Tuple<string, char, string[]>>();

        public void AddUngroupedColumnName(string groupKeyColumn, char delimiter, string[] groupConcatColumns) 
            => ungroupedColumns.Enqueue(new Tuple<string, char, string[]>(groupKeyColumn, delimiter, groupConcatColumns));

        public override List<List<DataElement>> Transform(List<string> headers, List<List<DataElement>> data) {
            // use with caution: not fully tested!
            while (ungroupedColumns.Count > 0) {
                Tuple<string, char, string[]> ungrouping = ungroupedColumns.Dequeue();
                bool[] isConcatColumn = (new List<string>(headers)).Select(entry => ungrouping.Item3.Contains(entry) ? true : false).ToArray();
                Func<DataElement, DataElement, DataElement> concat = (e1, e2) => new DataElement(e1.StringValue + ungrouping.Item2 + e2.StringValue);

                data = data
                    .GroupBy(row =>
                        row[headers.IndexOf(ungrouping.Item1)].StringValue,
                        (key, contents) => new { Key = key, Contents = contents})
                    .Select(group => new[] { new DataElement(group.Key) }
                        .Concat(group.Contents
                            .Aggregate((row1, row2) => {
                                for (int i = 0; i < row1.Count; i++) if (isConcatColumn[i]) row1[i] = concat(row1[i], row2[i]);
                                return row1;
                            }))
                        .ToList())
                    .ToList();
            }
            return data;
        }
    }
}