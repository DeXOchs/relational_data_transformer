using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace data_transformations {
    public class ObjectMapping : Mapping {
        private string[] iriRelevantColumns;
        private Func<string[], string> iriPath;
        private string objectType;
        private string[] relationIDs;
        
        /// <summary>
        /// ObjectMapping with multiple columns relevant for building an IRI and a named relation
        /// </summary>
        public ObjectMapping(string relationID, string[] iriRelevantColumns, Func<string[], string> iriPath, string objectType, string[] relationIDs,  string relationTarget)
            : base(relationID, relationTarget)
        {
            this.iriRelevantColumns = iriRelevantColumns;
            this.iriPath = iriPath;
            this.objectType = objectType;
            this.relationIDs = relationIDs;
        }

        /// <summary>
        /// ObjectMapping with multiple columns relevant for building an IRI but without a named relation (constructor for root node)
        /// </summary>
        public ObjectMapping(string relationID, string[] iriRelevantColumns, Func<string[], string> iriPath, string objectType, string[] relationIDs)
            : this(relationID, iriRelevantColumns, iriPath, objectType, relationIDs, "") {}
        
        /// <summary>
        /// ObjectMapping with a single column relevant for building an IRI and a named relation
        /// </summary>
        public ObjectMapping(string relationID, string iriRelevantColumn, Func<string, string> iriPath, string objectType, string[] relationIDs, string relationTarget)
            : this(relationID, new[]{ iriRelevantColumn }, list => iriPath(list[0]), objectType, relationIDs, relationTarget) {}

        /// <summary>
        /// ObjectMapping with a single column relevant for building an IRI but without a named relation (constructor for root node)
        /// </summary>
        public ObjectMapping(string relationID, string iriRelevantColumn, Func<string, string> iriPath, string objectType, string[] relationIDs)
            : this(relationID, new[] { iriRelevantColumn }, list => iriPath(list[0]), objectType, relationIDs, "") {}
        
        /// <summary>
        /// ObjectMapping for a virtual root node (will have an empty type, empty IRI and no named relation)
        /// </summary>
        /// <param name="relationID">Unique name for this relation, e.g. "VirtualRoot"</param>
        /// <param name="relationIDs">IDs of all relations of this virtual root (each can only be a RelationToObject)</param>
        public ObjectMapping(string relationID, string[] relationIDs)
            : this(relationID, new string[0], _ => string.Empty, string.Empty, relationIDs, string.Empty) {}

        public override List<DataElement> Map(in List<string> headers, List<DataElement> row) 
        {
            List<Relation> relations = new List<Relation>(relationIDs.Length);
            foreach (string relationID in relationIDs)
            {
                Relation relation = row[headers.LastIndexOf(relationID)].RelationValue;
                if (!relation.IsEmptyRelation) relations.Add(relation);
            }
            ReadOnlyCollection<string> h = headers.AsReadOnly();
            row.Add(new DataElement(new RelationToObject(
                relationTarget,
                new Subject(iriPath(iriRelevantColumns.Select(column => row[h.IndexOf(column)].StringValue).ToArray()), objectType, relations)
            )));
            return row;
        }
    }
}