using System;
using System.Collections.Generic;

namespace data_transformations {
    public class PropertyMapping : Mapping {
        private string columnName;
        private Func<string, string> valuePath;

        public PropertyMapping(string relationID, string columnName, string relationTarget)
            :this(relationID, columnName, relationTarget, value => value) {}

        public PropertyMapping(string relationID, string columnName, string relationTarget, Func<string, string> valuePath)
            : base(relationID, relationTarget) 
        {
            this.columnName = columnName;
            this.valuePath = valuePath;
        }

        public override List<DataElement> Map(in List<string> headers, List<DataElement> row) {
            row.Add(new DataElement(new RelationToProperty(
                relationTarget,
                valuePath(row[headers.IndexOf(columnName)].StringValue)
            )));
            return row;
        }
    }
}