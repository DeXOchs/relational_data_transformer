using System;
using System.Collections.Generic;
using System.Linq;

namespace data_transformations {
    public class ForeignKeyMapping : Mapping {
        private string columnName;
        private Func<string, string> valuePath;
        private char valueDelimiter;
        
        public ForeignKeyMapping(string relationID, string columnName, string relationTarget, Func<string, string> valuePath, Char valueDelimiter)
            : base(relationID, relationTarget)
        {
            this.columnName = columnName;
            this.valuePath = valuePath;
            this.valueDelimiter = valueDelimiter;
        }

        public ForeignKeyMapping(string relationID, string columnName, string relationTarget, Func<string, string> valuePath)
            : this(relationID, columnName, relationTarget, valuePath, char.MinValue) {}

        public override List<DataElement> Map(in List<string> headers, List<DataElement> row)
        {
            string[] values;

            string cellValue = row[headers.IndexOf(columnName)].StringValue;
            if (cellValue.Equals(string.Empty)) values = new string[0];
            else if (valueDelimiter.Equals(Char.MinValue)) values = new[] { valuePath(cellValue) };
            else values = cellValue.Split(valueDelimiter).Select(value => valuePath(value)).ToArray();

            row.Add(new DataElement(new RelationToForeignObject(relationTarget, values)));
            return row;
        }
    }
}