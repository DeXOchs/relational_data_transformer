using System.Collections.Generic;

namespace data_transformations {
    public abstract class Mapping {
        protected string relationID;
        protected string relationTarget;

        protected Mapping(string relationID, string relationTarget) {
            this.relationID = relationID;
            this.relationTarget = relationTarget;
        }

        public virtual List<string> MapHeaders(List<string> headers) {
            headers.Add(relationID);
            return headers;
        }
        public abstract List<DataElement> Map(in List<string> headers, List<DataElement> row);
    }
}