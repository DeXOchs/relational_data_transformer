﻿namespace data_transformations
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Transformer<string> transformer = new TestTransformer();

            ExportTransformation<string>.WriteToTXT("test_instances.ttl", transformer.Transform());
        }
    }
}
