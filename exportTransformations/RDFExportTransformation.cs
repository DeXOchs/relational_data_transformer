using System.Collections.Generic;
using System;
using System.Linq;

namespace data_transformations
{
    public class RDFExportTransformation : ExportTransformation<string> {
        private static readonly object lockingObject = new object();
        private static SynchronizedCollection<string> exportedSubjectIRIs = new SynchronizedCollection<string>(lockingObject);

        public override string Transform(List<Subject> data) 
        {
            Console.WriteLine($"{data.Count} items to export");
            DateTime start = DateTime.Now;
            
            string output = data.Select(subject => {
                Queue<Subject> queue = new Queue<Subject>(new[] { subject });
                string asString = String.Empty;
                while (queue.Count > 0) 
                {
                    Subject next = queue.Dequeue();
                    
                    if (!exportedSubjectIRIs.Contains(next.ResourceIRI) || next.IsVirtualNode) {
                        if (!exportedSubjectIRIs.Contains(next.ResourceIRI)) exportedSubjectIRIs.Add(next.ResourceIRI);
                        asString += ExportSubject(next, queue);
                    }
                }
                return asString;
            }).Aggregate((first, second) => first + second);

            DateTime end = DateTime.Now;
            Console.WriteLine($"{exportedSubjectIRIs.Count} subjects exported in about {end.Subtract(start).Seconds} seconds");
            return output;
        }

        private string ExportSubject(Subject subject, Queue<Subject> queue) {
            if (subject.IsVirtualNode) {
                subject.Relations.ForEach(relation => queue.Enqueue(((RelationToObject)relation).Value));
                return string.Empty;
            }
            else {
                return $"{subject.ResourceIRI}\na {subject.TypeIRI} ;\n"
                + subject.Relations.Select(relation => ExportRelation(relation, queue)).Aggregate(String.Empty, (first, second) => first + second)
                + " .\n";
            }
        }

        private string ExportRelation(Relation relation, Queue<Subject> queue)
        {
            Func<string, string, string, string> buildString = (string left, string text, string right) => $"{relation.Name} {left}{text}{right} ;\n";

            string output = String.Empty;
            if (relation is RelationToObject) 
            {
                Subject subject = ((RelationToObject)relation).Value;
                output += buildString(String.Empty, subject.ResourceIRI, String.Empty);
                queue.Enqueue(subject);
            }
            else if (relation is RelationToProperty) 
            {
                output += buildString("\"", ((RelationToProperty)relation).Value, "\"");
            }
            else if (relation is RelationToForeignObject) {
                ((RelationToForeignObject)relation).Values.ToList().ForEach(value => output += buildString(String.Empty, value, String.Empty));
            }
            return output;
        }
    }
}