using System.Collections.Generic;
using System.IO;
using System;

namespace data_transformations 
{
    public abstract class ExportTransformation<T> : Transformation 
    {
        public abstract T Transform(List<Subject> data);
        public static bool WriteToTXT(string filePath, string output) {
            Console.WriteLine($"Writing to TXT file ({output.Length} chars)");
            try {
                StreamWriter writer = new StreamWriter(filePath);
                writer.WriteLine(output);
                writer.Close();
            }
            catch (Exception e) {
                System.Console.WriteLine(e.Message);
                return false;
            }
            Console.WriteLine($"Wrote to TXT file ({output.Length} chars)");
            return true;
        }
    }
}